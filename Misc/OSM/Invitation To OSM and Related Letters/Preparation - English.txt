Those who would like to participate actively in Geo Tagging should do the following three preparatory steps.
We need two mobile apps which are available free on Playstroe.

1. From Playstore Install an app named as Vespucci. This is a small sized app. It will be installed in around one minute. 

Install Link :  

https://play.google.com/store/apps/details?id=de.blau.android  

You need not open this app immediately.

2. From Playstore Install an app named as OsmAnd. This is larger app than Vespucci. It will be installed in around three to four minutes.

Install Link :

https://play.google.com/store/apps/details?id=net.osmand&hl=en_IN&gl=US

After the installation open the app immediately. According to your location you will be prompted to download the map of your state (Maharashtra). Download the entire map of Maharashtra. It may take 5 to 10 minutes depending on your interenet speed. Open the map and ensure that it is loaded propely.
Then close the app.

3. Our tree tags will be directly uploaded to World Map. You need to open a free account on world map site. It is similar to opening facebook, whatsapp or instagram account. Visit openstreetmap.org using chrome or any other browser. Signup for a new account. Select user name and password. Confirm your credentals through email. Carefully write down your Username and Password at safe place.   





 
 
  
