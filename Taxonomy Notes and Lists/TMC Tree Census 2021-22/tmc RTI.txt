Please provide the electronic database record of all 722426 trees as per latest Tree Census (2017-22) conducted by Thane Municipal Corporation .

1.  At least the following 8 fields(columns) are required for each tree. These attributes have been documented in the census as per the published report..

        Scientific Name,  Marathi Common Name,  Girth , Height , Age, Latitude, Longitude, Canopy diameter (m)

2.  Data file(s) should be in xlsx or CSV or any other open data format.   

3.   Larger files may be split into smaller ones (for example based on ward), if they exceed attachment size limit 

3.  I would prefer the file(s) through email.
    
     My email ID  :    oak445@gmail.com   (  Alternative email ID :  milindoka@outlook.com   )
                        
4.  If you are sending the file(s) by USB stick, I am ready to pay the cost.


 Thank you
 
 Milind
