https://www.pmc.gov.in/en/open-data




Open Data Initiative:

PMC collects, processes and generates a large amount of data in its day-to-day functioning. But a large quantum of government data remains inaccessible to citizens, civil society, although most of such data may be non-sensitive in nature and could be used by public for social, economic and developmental purposes.

This data needs to be made available in an open format to facilitate use, reuse and redistribute; it should be free from any license or any other mechanism of control. Opening of government data in open formats would enhance transparency and accountability while encouraging public engagement. The government data in open formats has a huge potential for innovation building various types of Apps, mash-ups and services around the published datasets.
 
Open Data | Pune Municipal Corporation

Pune DataStore has been set up at http://opendata.punecorporation.org/ to provide collated access to Resources (datasets/apps) under Departments published in open format. It also provides a search & discovery mechanism for instant access to desired datasets. Pune DataStore also has a rich mechanism for citizen engagement. Besides enabling citizens to express their need for specific resource (datasets or apps) or API, it also allows them to rate the quality of datasets, seek clarification or information from respective data controller.

Pune DataStore Platform has a strong backend data management system which can be used by PMC departments to publish their datasets through a predefined workflow. They shall also have a dashboard to see the current status on their datasets, usage analytics as well as feedback and queries from citizens at one point. In future, the departments will also have an option to create visualization of their data.

Pune DataStore Platform will also feature a Communities component which facilitates forming of communities around datasets, domain of interest such as sanitation, education, health, or it could be app developers’ community or even of data journalists. This shall give first hand input to development community for building new components, apps. It shall also give input to departments as what kind of datasets is more useful and accordingly prioritize the release of the datasets. The key features are detailed as below:

Open Source Driven: Developed completely using Open Source Stack, facilitating cost saving in terms of software and licenses and also provisioning community participation in terms of further development of product with modules of data visualization, consumption, APIs to access datasets etc.

Metadata – Resources (Datasets/Apps) shall be published along with standard metadata along with controlled vocabularies on government sectors, jurisdictions, dataset types, access mode etc. Besides facilitating easy access to datasets, this shall be extremely useful in the future for federation/integration of data catalogs.

Social Media Connect–To support wider reach and dissemination of datasets, anyone can share the information about any dataset published on the platform with his/her social media pages on a press of a click.

Citizen Engagement – The Platform has also a strong component of Citizen Engagement. Citizens can express their views as well as embed the Resources (Datasets/Apps) in their blogs or web sites. Facility to contact the data administrator is also available on the Platform. Additionally the citizens can ask/ suggest for the required data set by clicking on ‘Suggest a Data Set’
 

Open Data | Pune Municipal Corporation

Please visit http://opendata.punecorporation.org/ for accessing the open data portal of Pune Municipal Corporation and share your views, feedback and dataset suggestions on info@punecorporation.org.

