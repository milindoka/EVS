Bauhinia racemosa
Family 	Caesalpinoideae
Common/Tamil Name 	Athi, Malai Athi, Vattathi
Uses: 	
Fuel (Calorific Value) 	Moderate
Fodder 	Moderate
Other Uses 	Wood forms a good fuel; inner bark yields a strong fibre from which durable ropes are made.
Seed Collection time 	November - January
No. of Seeds per kg 	8025/Kg.
Viability 	Upto 6 months
Germination percentage 	17%
Seed Treatment 	Soaking in cold water
Nursery Technique 	Pre-treated seeds are sown in the mother bed, seedlings can be pricked out and planted in the poly bags. Germination starts from 6th day onwards. 6 months old seedling perform well. In 30 x 45 cm size bags it attains 4 feet in 6 months.          .

