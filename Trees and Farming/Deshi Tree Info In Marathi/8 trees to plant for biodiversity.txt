8 trees to plant for biodiversity

When trees can give you fruit, flowers, medicines, birds, bees and more, why should you settle for less? Renee Vyas, known for the tree walks she conducts around Mumbai, tells Pooja Bhula about trees that are not just eye-candy, but also enable substantial biodiversity

Written By
Pooja Bhula

DNA
Updated: Jun 4, 2017, 03:21 PM IST


1.  Bahava tree

One of the most widespread of dry-and-moist deciduous trees in India, Bahava or Laburnums as they are called in English, have a lovely green-yellow foliage that looks even more spectacular when its yellow flowers bloom in late April. That's also what gives them the name 'golden showers'. By May they are at their peak and the rains induce more flushes till October. Although typically found in the stretch from Gujarat to Kerala, they are easy to grow in the north too. At one point there was a large number in South Mumbai, where an entire street has been named after them—Laburnum Street. This small-to-medium sized tree, having upward-growing branches, can grow upto 40ft, but are rather compact and adjust as per available space. But cramped is not their style either.

Why you should grow it: While its flowers attract insectivorous birds, bees come to suckle on the nectar and the common emigrant (a butterfly specie) lays its eggs on them, post which the caterpillars that are born feed on the leaves.


2. Taman tree

The wild child of the Western Ghats, Taman has purple blossoms that make the tree standout as a mass of a single, vibrant colour, earning it the nickname 'Queen's flowers'. Accorded the status of the state flower of Maharashtra, taman flowers blossom before the monsoons and return briefly post the rains. A sight to behold is the carpet of petals it leaves behind when autumn comes calling. Not compatible with extreme cold, the medium-sized tree can be grown anywhere except at very high altitudes.

Why you should grow it: Alluring for insects and birds alike, they are just as important to human beings because of their medicinal and healing properties.

3. Bakul tree

Slow to grow, but a pleasure to have, Bakul or Spanish cherry, has droopy branches and a dark, dense canopy. The middle-sized tree thrives in places offering warm and slightly moist climate. While it also needs well-drained soil like other deciduous trees, it can stand take some amount of water logging. Its ivory-coloured flowers that come out in summer and post monsoons are not only good to look at, but also fill the air with its fragrance and bear edible fruit.

Why you should grow it: As an evergreen tree, it purifies the air all year round, emits a lovely fragrance even when the flowers are dry and kadha made from its bark is good for improved dental hygiene.

4. Kadam tree

Quick to grow, tall (60 – 70 ft), the Kadam has a long, clean bole and spreads its branches to form a wide canopy, offering good shade. It bursts into a brilliant orange when flowering and bears tennis-ball-sized green fruit with rough skin. Native to deciduous forests, it requires full sun and shouldn't be planted on rocky soil that will stunt its shoot or restrict its roots.

Why you should grow it: It's a preferred perch for high-flying birds and offers food to Commander butterfly and common birds. The fruit can be also be eaten by humans.


5. Parijat tree

This sacred tree dedicated to Lord Krishna is filled with white flowers as the monsoon recedes, but they bloom only at night and fall in the morning to create lovely carpets. The one in Kintur village of Barabanki district in Awadh, UP, is believed to date back to the Mahabharata era. As per recent reports, the National Botanical Research Institute (NBRI) recently cured the ancient tree of decay, believed to have occurred due to offerings by worshippers, and now plans to take its saplings and create three clones of it in the same area. Generally growing upto just 20-30ft, it doesn't occupy much space, and can be planted at any place that offers full gaze of the sun and soil that's no too rocky.

Why you should grow it: The white flowers of this 'wish granting' tree are highly fragrant and have an orange centre containing nycthathin, traditionally boiled and used to create orange dyes to colour silk. Additionally, as per National Environment Engineering Research Institute (NEERI), its seeds, leaves and flowers are used to treat a wide range of ailments.

6. Sita Ashok tree

Small (30ft) and sensitive, this tree belonging to South India requires a fair amount of care initially—it should get some shade and must be watered every day. But it's worth the effort for the beautiful yellow-orange/red flowers that fill its dense crown from winter to pre-summer months. Seeds that it sheds make it easy to multiply. Worshipped by Hindus and Buddhists alike, it is believed to keep away sorrows.

Why you should grow it: As an evergreen tree the Sita Ashok is a perennial air-cleanser. Known to have several medicinal properties, it's used to make ayurvedic remedies like Ashokrishtra that's derived from its bark to treat menstrual irregularities, low digestion strength, etc.


7. Mango tree

The aam tree is not so aam. As per NEERI, fast-growing and erect, the mango tree is sturdy with rounded canopies that range from slender to broad. Known to have been with us at least since 4,000 years of recorded history, it fruits from May to July with every region in the country having its own variant. Our love for the 'king of fruits' is such that only a very small percentage gets exported. For commercial purposes, usually grafts are used, as it leads to quicker growth and to get fruit of the exact cultivar you want. But for non-commercial purposed you could just take the seed, put it in a plastic bag and once the sapling comes out, plant it in your compound. The tree will be much taller, live longer and won't require much maintenance. Although the fruit won't be of the same cultivar you had eaten, it will resemble either parent tree's offering and the taste of the mango will be just as delicious.

Why you should grow it: Next only to the banyan tree, this evergreen tree attracts all kinds of birds and butterflies. Its barks and branches are hosts to wild flowers, especially orchids and in the garden, they can offer sun-screen to shade-loving flora.

8. Jamun tree

Slow-growing jamun trees reach up heights of 90ft in the wild and can live more than 100 years. Hearty with dense foliage, they take up a good amount of space, but also offer great shade that's especially a blessing in the summer, when it's needed most. It's also the time of the year when it springs out lightly fragrant greenish-white flowers that eventually give way to mature, purple fruit.

Why you should grow it: Although the fruit, when it falls in large numbers, is considered a nuisance by many, it has anti-diabetic properties and the small size of the fruit makes it a favourite of little birds. High-perching birds and nocturnal aviators abound it too.

Planting Tips :  Unless mentioned otherwise, most of these trees are sturdy, require well-drained soil, can be planted anywhere and don't require much care. Sapling for these plants are reasonably priced and easily available at nurseries. Purchase and plant saplings that are 6-7 ft tall or three-years-old.  Water them thrice a week during summers and once a week during winters for about three years. In the fourth year, gradually reduce the frequency of watering and then completely stop. 

